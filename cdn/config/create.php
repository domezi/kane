<?php
if(isset($_POST["schoolname_shortcut"])) {

   $schoolname=str_replace(" ",null,str_replace(".",null,str_replace("/",null,$_POST["schoolname_shortcut"])));$schoolname.="_".uniqid();
   if(is_file($schoolname.".json"))$schoolname.="_".uniqid();

   if(!strlen($_POST["json"])) {
                                
                        $school_config["conf"]["lang"]="de"; // recommended: $school_config["conf"]["lang"]="de"; || Possible values: en, de, fr, es

                        /*
                        * Do you want to have your configuration on a server, so all the vplan touchscreens can be configured at once? please 
                        * change settings in this file once. Then do the stuff explained in "README.md".- Now upload the file school_config.json 
                        * to your server which is supposed to become a config server. Then come back to this file and change the value of
                        * $school_config["conf"]["conf_server_path"] to the file, e.g: https://www.example.com/direcory/school_config.json
                        * Then rerun the steps in README.md and your done. (Now the contents of school_config.json changed again). If you now have
                        * a second vplan touchscreen, please just copy the newer school_config.json, which is located in the same directory as this file,
                        * to your second touchscreen and so on...
                        */
                        $school_config["conf"]["conf_server_path"]="http://vplantouch.com/cdn/config/".$schoolname.".json"; // set false, if you want to use the config, used in this file

                        if($school_config["conf"]["conf_server_path"]=="") {

	                        $school_config["dash"]["name"]["css"]["font-size"]="70px";
	                        $school_config["dash"]["logo"]["src"]="config/img/logo.png";
	                        $school_config["dash"]["name"]["html"]=$_POST["schoolname_full"];

	                        $pin=$_POST["teacher_pass"];
	                        $school_config["dash"]["password"]["teacher"]=hash("sha512","0MjM5dTg5F03101998dTJuFog5VSg9KVovJlRSejk4cjM0cj187QzMnI0".$pin."VCYvUsKnCWi8oIlpSPVUoKSGHSELCp1UoKVJ1N42DM5dXI");
	                        $pin=null;

	                        /*division of forms*/

	                        $school_config["home"]["tiles"]["forms"][1]["html"]="&nbsp;";
	                        $school_config["home"]["tiles"]["forms"][1]["regex"]="";

	                        $school_config["home"]["tiles"]["forms"][2]["html"]="&nbsp;";
	                        $school_config["home"]["tiles"]["forms"][2]["regex"]="";

	                        $school_config["home"]["tiles"]["forms"][3]["html"]="&nbsp;";
	                        $school_config["home"]["tiles"]["forms"][3]["regex"]="";

	                        $school_config["home"]["tiles"]["forms"][4]["html"]="Mehr";

	                        $school_config["home"]["tiles"]["forms"]["auto_select_page"]=0; //0(default)=disabled, 1-4=show page N

	                        /*division of teachers*/
	                        $school_config["home"]["tiles"]["teachers"][1]["html"]="A-G";
	                        $school_config["home"]["tiles"]["teachers"][1]["regex"]="/[a-g]/i";

	                        $school_config["home"]["tiles"]["teachers"][2]["html"]="H-K";
	                        $school_config["home"]["tiles"]["teachers"][2]["regex"]="/[h-k]/i";

	                        $school_config["home"]["tiles"]["teachers"][3]["html"]="L-R";
	                        $school_config["home"]["tiles"]["teachers"][3]["regex"]="/[l-r]/i";

	                        $school_config["home"]["tiles"]["teachers"][4]["html"]="S-Z";
	                        $school_config["home"]["tiles"]["teachers"]["auto_select_page"]=0; //0(default)=disabled, 1-4=show page N

	                        /*division of rooms*/ /*ml25*/
	                        $school_config["home"]["tiles"]["rooms"][1]["html"]="Gewerbliche";
	                        $school_config["home"]["tiles"]["rooms"][1]["regex"]="/\b[0-9]/i";

	                        $school_config["home"]["tiles"]["rooms"][2]["html"]="Hauswirtschaft";
	                        $school_config["home"]["tiles"]["rooms"][2]["regex"]="/\bh/i";

	                        $school_config["home"]["tiles"]["rooms"][3]["html"]="C / Werkstätten";
	                        $school_config["home"]["tiles"]["rooms"][3]["regex"]="/\b[wc]/i";

	                        $school_config["home"]["tiles"]["rooms"][4]["html"]="Mehr";

	                        $school_config["home"]["tiles"]["rooms"]["auto_select_page"]=0; //0(default)=disabled, 1-4=show page N

	                        /* timetable config */
	                        $school_config["plugins"]["timetable"]["skip_2nd_periods"]=false; // == true if your only having double periods   || Recommended value: FALSE
	                        $school_config["plugins"]["timetable"]["show_teachers_email_adress"]=false; // == true if your only having double periods   || Recommended value: FALSE

	                        // this is, if you want to force times to be displayed, even if there is no period in the whole week | otherwise, just delete this line or set it =null
	                        $school_config["plugins"]["timetable"]["period_times_always_visible"]=array(); 
	
	                        // codes which are allowed to access config
	                        $school_config["plugins"]["qrcode"]["mastercodes"]=array(""); 

	                        $school_config["conf"]["pinlock_enabled"]=($_POST["teacher_pass"]>1)?true:false;

                        }

                        $school_config["meta"]["ts"]=time();
                        $school_config["meta"]["v"]=$v;
           $_POST["json"]=json_encode($school_config);
    }



    if(strlen(json_encode(json_decode(str_replace("<?",null,$_POST["json"]))))>7){
        file_put_contents($schoolname.".json",json_encode(json_decode(str_replace("<?",null,$_POST["json"]))));
        echo "<h1>Wurde erstellt</h1><p>Pfad:<input style=width:500px value='http://vplantouch.com/cdn/config/".$schoolname.".json'></p>";
    } else echo "<h1>Fehler.</h1><p>Es wurde kein JSON erkannt</p><a href=?>zurück</a>";
} else {
    if($_GET["type"]=="einfach") {
        echo "
            <h1>Neue Config erstellen</h1><br>Bitte ausfüllen:<br>
            <form method=post>
            <br>Schulname:<br>
            <input placeholder=schoolname name=schoolname_full>
            <br>Schulname Kurz (nur kleinbuchstaben, 3-10 zeichen):<br>
            <input placeholder=schoolname_shortcut name=schoolname_shortcut><br>
            Sprache:<br>
            <select name=lang>
                <option value=de>Deutsch</option>
                <option value=en>English</option>
                <option value=es>Spanisch</option>
                <option value=fr>Französisch</option>
            </select><br>
            <input name=teacher_pass type=number min=0 max=9999 placeholder='Pin (leer lassen zum deaktivieren)'> <br>
            Mastercode (Barcode, der zur OnScreen Konfigurierung gescannt werden muss)<br>
            <input name=mastercode type=number placeholder='mastercode einscannen'><br>
            <input name=skip_2nd_periods type=checkbox> Alles nur Doppelstunden (nicht empfohlen)<br>
            <div></div>
            <input type=submit>
            </form></div>
        ";
    } elseif($_GET["type"]=="schwer") {
        echo "
            <h1>Neue Config erstellen</h1><p>Bitte ausfüllen</p>
            <form method=post>
            <input placeholder=schoolname name=schoolname_shortcut><br>
            <textarea name=json placeholder=json></textarea><br>
            <input type=submit>
            </form></div>
        ";
    } else {
        echo "<h1>Neue Config erstellen</h1>
            <div>Bitte wählen: <a href=http://vplantouch.com/clients/config/create/simple/>Einfacher Modus(empfohlen)</a> | <a href=http://vplantouch.com/clients/config/create/advanced/>JSON Code eingeben(fortgeschritten)</a></div>
            <hr>";
    }
}
