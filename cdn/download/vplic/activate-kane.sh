#!/bin/bash
version=1.2.2
if [  "$(whoami)" != "root" ] ; then zenity --info --text="Bitte führen Sie dieses Programm als Administrator aus!" ; exit 1 ; fi

pkill zenity 
key=$( zenity --entry --title="Kane Aktivieren (V$version)" --text="Bitte geben Sie einen gültigen Lizenzschlüssel ein:"  )

zenity --progress \
  --text "Prüfen..."\
  --percentage=3 \
  --pulsate \
  --no-cancel &

sleep 5
pkill zenity

total_ram=`grep MemTotal /proc/meminfo | awk '{print $2}'`
macs=`cat /sys/class/net/*/address`


trans=$( echo -n "90YWxfcmFtIyMjNjA6NDU6Y2I6OWE6YmM6$key###key###$total_ram###total_ram###$macs" | base64 )


res=$( wget -qO- "https://softatomos.com/kane/sites/activation-checkkey.php?key=2Nzk4NTQ3MzhmZEZkamY4ZGZhc2QwZjlzZ$trans" )



if [ $res == "MjIzYwOjQ1OmNiOjlhOmJjOjE2CjAwO" ] ; then echo "Key is valid!"; 


    zenity --progress \
      --text "Aktivierung..."\
      --percentage=3 \
      --pulsate \
      --no-cancel &

    echo \#d$total_ram#o$macs#m#i | md5sum > /etc/cache.d.md5
    chmod 755 /etc/cache.d.md5

    sleep 5
    pkill zenity
    zenity --info --text="Aktivierung erfolgreich, bitte starten Sie den Rechner jetzt neu!"

else
    
    zenity --error --text="Dieser Lizenzschlüssel ist ungültig oder ist bereits aktiviert. Kane Lizenz wurde nicht installiert!" ; ./$0; exit 1 ;

fi
