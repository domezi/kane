#!/bin/bash

#(c)2016 Dominik Ziegenhagel (www.ziegenhagel.com)
#default user pass: k4n34567890			

















version="4.3.6"

																																										
#conf
site="https://soft-atomos.com/kane"
cdnroot=$site"/cdn/download/"
tempdir="/tmp/vplantouch/"
cdnpath=$cdnroot"vpinst/res/"
zipfile="vplan_touch_v"$(curl --silent  $site/cdn/download/source/version/)".zip"
mozillafile="mozilla.zip"
installdir="/var/www/html/"
dbfile="vplan_touch/bin/dbcon.php"
sqlpath="vplan_touch/sql/"
sqlfile="webscheduler_structure.sql"
database="webscheduler"
lockfile="$tempdir"".lock"
maindlpath=$cdnroot"source/version/"$zipfile
updatedbfile="vplan_updatedb.zip"
vplanupdatedb=$site"/cdn/download/source/vplan_updatedb.zip"


pkill zenity
	
if [  "$(whoami)" != "root" ] ; then zenity --info --title="Kane Installations-Assistent V$version""$aktivierungstext" --text="Bitte führen Sie dieses Programm als Administrator aus!" ; exit 1 ; fi
echo "checking for internet connection..."
wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "[OK]"
else
    echo "[Failed]"
    echo "You're offline. Please connect to the internet" 
    pkill zenity
    zenity --error --title="Kane Installations-Assistent V$version""$aktivierungstext" --text="Bitte überprüfen Sie Ihre Internetverbindung!"
    exit
fi
		

# general actions
case $1 in
   noupdate)    
        zenity --progress \
          --text "Vorbereiten..."\
          --title "Kane Installations-Assistent V$version""$aktivierungstext"\
          --width=350 \
          --pulsate \
          --no-cancel &
        echo "Skipping update v: "$version
     ;;
   *) 
        zenity --progress \
          --text "Nach Aktualisierungen suchen..."\
          --title "Kane Installations-Assistent V$version""$aktivierungstext"\
          --percentage=3 --width=350 \
          --pulsate \
          --no-cancel &

       



        cd /tmp
        wget -c $site/cdn/download/vpinst/kane.sh
        chmod +x kane.sh
        sudo mv kane.sh /usr/bin/kane && sudo kane noupdate
        exit
    ;;

esac

function strstr ( )
{
  echo $1 | grep --quiet $2
}

clear

echo "-----------------------------------------------------------------------"
echo "Kane Installer by Dominik Ziegenhagel 2017 (www.ziegenhagel.com)"
echo "Version: $version"
echo "Temporary dir: $tempdir"
echo "-----------------------------------------------------------------------"
echo






sudo apt install zenity gawk -y


sudo apt install  debconf-utils -y


function version { echo "$@" | gawk -F. '{ printf("%03d%03d%03d\n", $1,$2,$3); }'; }
echo "checking for newer versoin"
serverversion=$(curl --silent  $site/cdn/vpinst.v)
if [ "$(version "$version")" -lt "$(version "$serverversion")" ]; then

			cd /tmp
			wget -c $site/cdn/download/vpinst/kane.sh
			chmod +x kane.sh
			sudo mv kane.sh /usr/bin/kane && sudo kane
			
			

#		if [ -f /tmp/vpinst_auto_update ]; then
#			read -p  "> there is a newer version of vpinst avaiable, may i update? (y/n)" ifupdate
#		else
#			cd /tmp
#			wget -c $site/cdn/download/vpinst/vpinst.sh
#			chmod +x vpinst.sh
#			sudo mv vpinst.sh /usr/bin/vpinst && sudo vpinst
#		fi


#		if [[ $ifupdate == "y" ]]; then
#
#			cd /tmp
#			wget -c $site/cdb/download/vpinst/vpinst.sh
#			chmod +x vpinst.sh
#			sudo mv vpinst.sh /usr/bin/vpinst && sudo vpinst
#
#		else
#			echo "exiting"
#			exit
#		fi


	exit
else
	echo "[OK]"
fi





#handling temporary dir
if [ -d "$tempdir" ]; then
	rm -r $tempdir
fi

mkdir $tempdir
cd $tempdir





                    ### activation CHECK =======================================================================================================================================================================================

                    activated=false
                    aktivierungstext=" [Demo-Version]"

                    a=`grep MemTotal /proc/meminfo | awk '{print $2}'`
                    b=`cat /sys/class/net/*/address`
                    c=`echo \#d$a#o$b#m#i | md5sum`
                    d=`cat /etc/cache.d.md5`

                    if [ "$d" == "$c" ]; then
                        activated=true
                        aktivierungstext=" [Aktiviert]"
                    fi


                    ### activation INSTALLATION =======================================================================================================================================================================================

                    if [[ $activated == false ]]; then

                        pkill zenity

                        if zenity --icon-name=system-software-install --title="Kane Installations-Assistent V$version""$aktivierungstext" --question --text="\
Dieser Assistent wird zur Installation von Kane 
verwendet. Klicken Sie auf 'Demo-Version', falls 
Sie keinen Lizenzschlüssel besitzen, und Kane 
testen möchten. Falls Sie einen Lizenzschlüssel 
besitzen können Sie auf 'Vollversion installieren' 
klicken, und Kane direkt aktivieren. " --ok-label="Vollversion installieren" --cancel-label="Demo-Version";
                        then

                            key=$( zenity --height=200  --entry --title="Kane Installations-Assistent V$version""$aktivierungstext" --text="Bitte geben Sie einen gültigen Lizenzschlüssel ein:"  )

                            zenity --progress \
                              --text "Prüfen..."  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
                              --percentage=3 --width=350 \
                              --pulsate \
                              --cancel-label=nah \
                              --no-cancel &

                            sleep 2
                            pkill zenity

                            total_ram=`grep MemTotal /proc/meminfo | awk '{print $2}'`
                            macs=`cat /sys/class/net/*/address`

                            trans=$( echo -n "90YWxfcmFtIyMjNjA6NDU6Y2I6OWE6YmM6$key###key###$total_ram###total_ram###$macs" | base64 )

                            res=$( wget -qO- "https://soft-atomos.com/kane/sites/activation-checkkey.php?key=2Nzk4NTQ3MzhmZEZkamY4ZGZhc2QwZjlzZ$trans" )

                            if [ $res == "MjIzYwOjQ1OmNiOjlhOmJjOjE2CjAwO" ] ; then echo "Key is valid!";


                                zenity --progress \
                                  --text "Aktivierung..."  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
                                  --percentage=3 --width=350 \
                                  --pulsate \
                                  --no-cancel &

                                #aktivierungs notification
                                zenity --notification --text="Kane Installations-Assistent V$version""$aktivierungstext\nDer Lizenzschlüssel von Kane wurde erfolgreich auf diesem Rechner installiert."



                                echo \#d$total_ram#o$macs#m#i | md5sum > /etc/cache.d.md5
                                chmod 755 /etc/cache.d.md5

                                activated=true
                                aktivierungstext=" [Aktiviert]"

                                sleep 5
                                pkill zenity

                            else
                                
                                zenity --error  --title="Kane Installations-Assistent V$version""$aktivierungstext" --text="Dieser Lizenzschlüssel '$res' ist ungültig oder ist bereits aktiviert.\nKane Lizenz wurde nicht installiert!" ; ./$0; exit 1 ;

                            fi
                        fi
                    fi

                    ### end of activation stuff ==============================================================================================================================================================================








pkill zenity
ans=$(zenity  --list --height=270 --width=500 --cancel-label="Installation abbrechen" --title="Kane Installations-Assistent V$version""$aktivierungstext"  --text "Bitte wählen Sie die zu Installierende Bereiche aus, und klicken Sie auf 'Weiter'." --checklist --ok-label=Weiter  --column "Installieren?" --column "Beschreibung" TRUE "Generelles Paket-Update" TRUE "Benötigten Bibliotheken installieren" TRUE "Kane TouchWiz und Komponenten installieren"   TRUE "Konfiguration überschreiben"  TRUE "Sandbox einrichten"     FALSE "PHPMyAdmin einrichten"                  --separator=";");

if [[ $ans == "" ]];then exit 1;fi

#konfiguration====================================================================================================================================================================
if $( echo $ans | grep --quiet 'Generelles Paket-Update' );then packageupdate=true;fi
if $( echo $ans | grep --quiet 'Benötigten Bibliotheken installieren' );then installlibraries=true;fi
if $( echo $ans | grep --quiet 'Kane TouchWiz und Komponenten installieren' );then kanetouchwiz=true
kaneupdatedat=true
databasesetup=true;fi
if $( echo $ans | grep --quiet 'Konfiguration überschreiben' );then createconfig=true;fi
if $( echo $ans | grep --quiet 'Sandbox einrichten' );then sandbox=true;fi
if $( echo $ans | grep --quiet 'PHPMyAdmin einrichten' );then phpmyadmin=true;fi
#endekonfiguration================================================================================================================================================================
#[[ `echo $ans | cut -d \; -f 1` == "Generelles Paket-Update" ]] && packageupdate=true
#[[ `echo $ans | cut -d \; -f 2` == "Benötigten Bibliotheken installieren" ]] && installlibraries=true
#[[ `echo $ans | cut -d \; -f 3` == "Kane TouchWiz installieren" ]] && kanetouchwiz=true
#[[ `echo $ans | cut -d \; -f 4` == "Kane UpdateDat installieren" ]] && kaneupdatedat=true
#[[ `echo $ans | cut -d \; -f 5` == "Datenbank-Struktur aufsetzen" ]] && databasesetup=true
#[[ `echo $ans | cut -d \; -f 6` == "Sandbox einrichten" ]] && sandbox=true
#endekonfiguration================================================================================================================================================================




echo "Folgende Einstellungen werden installiert"
echo "packetupdate:"; if [[ $packageupdate == true ]];then echo "JA"; else echo "NEIN"; fi
echo "installlibraries:"; if [[ $installlibraries == true ]];then echo "JA"; else echo "NEIN"; fi
echo "kanetouchwiz:"; if [[ $kanetouchwiz == true ]];then echo "JA"; else echo "NEIN"; fi
echo "kaneupdatedat:"; if [[ $kaneupdatedat == true ]];then echo "JA"; else echo "NEIN"; fi
echo "databasesetup:"; if [[ $databasesetup == true ]];then echo "JA"; else echo "NEIN"; fi
echo "sandbox:"; if [[ $sandbox == true ]];then echo "JA"; else echo "NEIN"; fi




#read -p "> May I run update and upgrade first (recommended) ? (y/n)" proceed
if [[ $packageupdate == true ]];
then


    zenity --progress \
      --text "Pakete werden heruntergeladen..."  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
      --percentage=3 --width=350 \
      --pulsate \
      --no-cancel &

	sudo apt-get update && sudo apt-get upgrade -y
fi 

pkill zenity
zenity --progress \
  --text "Installation..." --title="Kane Installations-Assistent V$version""$aktivierungstext"\
  --percentage=3 \
  --pulsate \
  --no-cancel &


#read -p "> May I install packages (required) ? (y/n)" proceed
if [[ $installlibraries == true ]];
then
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password k4n34567890'
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password k4n34567890'

	sudo apt-get install wget openssh-server apache2 php7.0 mysql-server php7.0-curl php7.0-zip curl vim php libapache2-mod-php php-mcrypt php-mysql php7.0-xml -y
fi


if [[ $phpmyadmin == true ]]
then

    zenity --info --title="Kane Installations-Assistent V$version""$aktivierungstext" --text="Bitte konfigurieren Sie die PHPMyAdmin Installations-Einstellungen im Terminal, sobald Sie danach gefragt werden!" ; 

	sudo apt-get install phpmyadmin -y

    pkill zenity
    zenity --progress \
      --text "Server wird neugestartet..." --title="Kane Installations-Assistent V$version""$aktivierungstext"\
      --percentage=3 \
      --pulsate \
      --no-cancel &

	
	sudo echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
	echo "[OK]"
	echo "include written to /etc/apache2/apache2.conf"
	
	echo "restarting apache ..."
	sudo service apache2 restart
	echo "[OK]"
	
fi 


notcorrect=true;

hostname="localhost"
username="root"
password="k4n34567890"

if mysql -u$username -p$password  -e 'show databases'; then
    notcorrect=false
else
	echo "[Failed] Couldn't log into mysql, please check your connection data"
fi

while [ $notcorrect ]
do
    
	echo "Please enter mysql connection data, press Enter for default value.";
	#read -p "> hostname (default=localhost) :" hostname
	#read -p "> username (default=root)      :" username
	#read  -s -p "> password (not visible while typing):" password
    pkill zenity
    IN="`zenity --forms  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
	    --text="Bitte geben Sie die Datenbank Verbindungsdaten ein" \
	    --separator=";" \
	    --add-entry="Server (localhost)" \
	    --add-entry="Benutzer (root)"  --ok-label="Weiter" --cancel-label="Testen" \
	    --add-password="Passwort"`"
           
    hostname=`echo $IN | cut -d \; -f 1`
    username=`echo $IN | cut -d \; -f 2`
    password=`echo $IN | cut -d \; -f 3`
	
	[[ $hostname == "" ]] && hostname="localhost"
	[[ $username == "" ]] && username="root"
	[[ $password == "" ]] && password="password"

	if mysql -u$username -p$password  -e 'show databases'; then
	    notcorrect=false
		 break;
	else
		echo "[Failed] Couldn't log into mysql, please check your connection data"
	fi
	
	
done







#read -p "> Do you want me to download VPlan Touch (recommended) ? (y/n)" proceed
if [[ $kanetouchwiz == true ]];
then


   	mv /var/www/html/vplan_touch/config/ /var/www/html/vplan_touch/tmp_config/ 
	
	barcodesdynamic="/var/www/html/vplan_touch/var/barcodes_dynamic.json_""$RANDOM";
	echo "creating backup of barcode files..."

	cp /var/www/html/vplan_touch/var/barcodes_dynamic.json $barcodesdynamic
	
	echo "Enviroment set up. Getting Kane ..."
	

    zenity --progress \
      --text "Kane TouchWiz wird heruntergeladen..." --title="Kane Installations-Assistent V$version""$aktivierungstext"\
      --percentage=3 --width=350 \
      --pulsate \
      --no-cancel &

	wget -c $maindlpath

    pkill zenity
	
	echo "Checking file"
	size=$( stat -c %s $zipfile)
	
	if [[ size -gt 50000 ]]; then
		echo "[OK]"
	else
		echo "[Failed]"
		exit
	fi
	
	rm /var/www/html/index.html
	
	echo "starting unzipping $installdir"
	unzip -o $zipfile -d $installdir
	echo "[OK]"

	echo "owning vplan_touch root to www-data"
	chown www-data -R /var/www/html/vplan_touch/
	
	
	echo "writing db connection data..."
	echo "<?php \$db=mysqli_connect(\"$hostname\",\"$username\",\"$password\",\"$database\"); ?>" > "$installdir$dbfile"
	echo "[OK]"
	
	rm  /var/www/html/vplan_touch/var/barcodes_dynamic.json
	mv $barcodesdynamic /var/www/html/vplan_touch/var/barcodes_dynamic.json


    #read -p "> Use given config? (First time installing? Type 'n'. You changed the configuration manually before and want to keep it? Then type 'y' (y/n)" proceed
    if [[ $createconfig == true ]];
    then
        rm -r /var/www/html/vplan_touch/config/
        mv /var/www/html/vplan_touch/tmp_config/ /var/www/html/vplan_touch/config/ 
    else
	   # read -p "> Which language do you want your GUI? (de/en/fr/es)?" lang

        lang2=$(zenity  --title="Kane Installations-Assistent V$version""$aktivierungstext" --cancel-label="Auto" --ok-label="Weiter" --width=500 --height=220 --list  --text "Bitte wählen Sie die Sprache der Oberfläche aus:" --radiolist  --column "OK" --column "Sprache" TRUE Deutsch en  Englisch fr Französisch es Spanisch); 

        lang="de"
	    [[ $lang2 == "Englisch" ]] && lang="en"
	    [[ $lang2 == "Spanisch" ]] && lang="es"
	    [[ $lang2 == "Französisch" ]] && lang="fr"
	    echo '<?php $school_config["conf"]["lang"]="'$lang'"; ?>' > /var/www/html/vplan_touch/config/tmp.school_config_generate_file_insert.php
	    	
	    echo '<?php $school_config["conf"]["conf_server_path"]=""; ?>' >> /var/www/html/vplan_touch/config/tmp.school_config_generate_file_insert.php
	    	
	    wget -qO- http://localhost/vplan_touch/config/school_config_generate_file.php?firstinstall &> /dev/null
    fi

fi

#read -p "> Do you want me to install VPlan UpdateDB for getting the data from a server and VPlan ServerDB to host an API? (y/n)" proceed
if [[ $kaneupdatedat == true ]];
then

	
	echo "owning vplan_touch root to www-data"
	chown www-data -R /var/www/html/vplan_updatedb/
	
	

	cd /var/www/html
	
	rm vplan_updatedb.zip

	wget -c $vplanupdatedb

	echo "getting "$vplanupdatedb
	
	echo "Checking file"
	sizeb=$( stat -c %s "vplan_updatedb.zip")
	
	if [[ sizeb -gt 10000 ]]; then
		echo "[OK]"
	else
		echo "[Failed]"
		exit
	fi
	
	echo "starting unzipping $updatedbfile"
	unzip -o $updatedbfile -d $installdir
	echo "[OK]"
	
	echo "owning vplan_updatedb root to www-data"
	chmod -R 777 /var/www/html/vplan_updatedb/
	chown www-data -R /var/www/html/vplan_updatedb/
	

	echo "checking crontab... Not found. adding crontab entry..."
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php &> /dev/null'>/tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php &> /dev/null'>>/tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php &> /dev/null'>>/tmp/vplantouch/cron_vplantouch_update
	echo "">>/tmp/vplantouch/cron_vplantouch_update
	chmod +x /tmp/vplantouch/cron_vplantouch_update
	crontab -u vplantouch /tmp/vplantouch/cron_vplantouch_update

	
	
    IN="`zenity --forms  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
	    --text="Bitte geben Sie die Untis Verbindungsdaten ein" \
	    --separator=";" --cancel-label="Überspringen" --ok-label="Weiter" \
	    --add-entry="Server (https://erato.webu...)" \
	    --add-entry="Benutzer (ghse15)" \
	    --add-password="Passwort"`"

           
    untisurl=`echo $IN | cut -d \; -f 1`
    untisusername=`echo $IN | cut -d \; -f 2`
    untispassword=`echo $IN | cut -d \; -f 3`
	

#	echo "Please enter untis api connection data, press Enter for default value.";
#	read -p "> url (default=https://erato.webuntis.com/WebUntis/jsonrpc.do?school=ghse) :" untisurl
	[[ $untisurl == "" ]] && untisurl="https://erato.webuntis.com/WebUntis/jsonrpc.do?school=ghse"

#	read -p "> username (default=ghse15)      :" untisusername
	[[ $untisusername == "" ]] && untisusername="ghse15"
#	read -s -p "> password (not visible while typing):" untispassword
	
	
	

	#read -p "> (optional) Do you want to addtionally save login data for retrieving the api connection data from a json file ? (y/n):" proceed
	
   # if zenity --question --text="Möchten Sie einen Datenserver einbinden?" --ok-label="Ja" --cancel-label="Nein (Empfohlen)";
	#then
	#	read -p "> url (default=https://www.ghse.de/vpapp/ghse.json) :" untisurl2
	#	read -p "> username (default=ghse15)      :" untisusername2
	#	read -s -p "> password (not visible while typing):" untispassword2
	#	[[ $untisurl2 == "" ]] && untisurl2="https://www.ghse.de/vpapp/ghse.json"
	#	[[ $untisusername2 == "" ]] && untisusername2="ghse15"
	

	#fi



	echo "{\"Database\": {\"Username\": \"$username\",\"Password\": \"$password\",\"Database-Name\": \"$database\",\"Host\":\"$hostname\"},\"Data-Server\": {\"Url\":\"$untisurl\",\"Username\": \"$untisusername\",\"Password\": \"$untispassword\"}}" > "$installdir""vplan_updatedb/etc/logins.json"




awk '

/<Directory/, /<\/Directory>/ {


                if ( $0 == "<Directory /var/www/>" )            # Eintrag gefunden, um den es geht.
                {
                        DOIT="yes" ;                            # Merken !
                        printf "found line %d\n", NR;           # debug
                }

                if ( DOIT == "yes" )
                {
                        res = sub("AllowOverride None", "AllowOverride AuthConfig", $0 ) ;      # res = 0, wenn nicht ausgetauscht wurde, sonst 1
                        if ( res == 1 ) # Austausch hat stattgefunden
                        {
                                printf "exchanged line %d / %d\n", NR, res; # debug
                                DOIT="no" ;
                        }
                }

        print

        next
}

{ print ; }
' /etc/apache2/apache2.conf > mktemp

diff /etc/apache2/apache2.conf mktemp

echo cp mktemp /etc/apache2/apache2.conf

rm mktemp

sudo a2enmod rewrite
sudo service apache2 restart

	
fi




#read -p "> Do you want me to setup the database structure (highly recommended)? (y/n)" proceed
if [[ $databasesetup == true ]];
then
	
	cd $installdir$sqlpath
	
	mysql -u$username -p$password -e "drop database $database";
	mysql -u$username -p$password -e "drop database webschedulertmp";
	mysql -u$username -p$password -e "create database $database";
	mysql -u$username -p$password -e "create database webschedulertmp";
	mysql -u$username -p$password --database=$database -e "source $sqlfile";
	mysql -u$username -p$password --database=webschedulertmp -e "source $sqlfile";
	
	echo "[OK]"

fi 


cd $tempdir


#installatino complete

#read -p "> Do you want me to turn your computer into a VPlan Touch PC?(recommended for production state) (y/n)" proceed
if [[ $sandbox == true ]];
then
		
		
	ret=true
	getent passwd vplantouch >/dev/null 2>&1 && ret=false
	
	if $ret; then
	
		#echo "I'll set up a new user named vplantouch, please choose a password for this user when youre being asked for it."
		
		sudo useradd vplantouch
		echo vplantouch:k4n34567890 | chpasswd
				
	fi	
		




	echo "checking crontab... Not found. adding crontab entry..."
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php &> /dev/null' > /tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php &> /dev/null' >> /tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php &> /dev/null' >> /tmp/vplantouch/cron_vplantouch_update
	echo "" >> /tmp/vplantouch/cron_vplantouch_update
	chmod +x /tmp/vplantouch/cron_vplantouch_update
	crontab -u vplantouch /tmp/vplantouch/cron_vplantouch_update

	pkill firefox
	
	cd /home/vplantouch
	
	mv .mozilla .mozilla_
	

    zenity --progress \
      --text "Sandbox wird aufgesetzt..."  --title="Kane Installations-Assistent V$version""$aktivierungstext"\
      --percentage=3 --width=350 \
      --pulsate \
      --no-cancel &

	wget -c "$cdnpath$mozillafile"
	unzip -o $mozillafile
	rm $mozillafile
	chown -R vplantouch .mozilla
	
	wget -c https://soft-atomos.com/vplantouch/cdn/download/vpinst/res/firefox_bootstrap.sh.zip
    unzip -o firefox_bootstrap.sh.zip
	chmod +x .firefox_bootstrap.sh
	chown -R vplantouch .firefox_bootstrap.sh
	
	echo "writing config"
	cd /home/vplantouch
	mv .config .config_
	wget -c "$cdnpath""config.zip"
	unzip -o config.zip
	rm config.zip
	chown -R vplantouch .config
	
    pkill zenity
    zenity --progress \
      --text "Einrichtung wird abgeschlossen..." --title="Kane Installations-Assistent V$version""$aktivierungstext"\
      --percentage=3 --width=350 \
      --pulsate \
      --no-cancel &


	echo "setting up autologin"
	echo "
[daemon]

AutomaticLoginEnable=true
AutomaticLogin=vplantouch

[security]

[xdmcp]

[gui]

[greeter]

[chooser]

[debug]

[servers]
">>/etc/mdm/mdm.conf 


	cd $tempdir
	
	echo "changing background"
	wget -c "$cdnpath""bg.jpg"
	
	cp "bg.jpg" /usr/share/backgrounds/linuxmint/linuxmint.jpg
	cp "bg.jpg" /usr/share/backgrounds/linuxmint/default_background.jpg

	echo "removing desktop icons"
	
	echo "setting bot logo"
	wget -c "$cdnpath""logo.png"
	sudo apt-get install startupmanager plymouth plymouth-drm plymouth-themes-mint -y
	sudo /usr/sbin/plymouth-set-default-theme mint-logo
	cp logo.png /lib/plymouth/themes/mint-logo/logo.png
	sudo update-initramfs -u
	

	mv Desktop Desktop_
	mv Schreibtisch Schreibtisch_
	
	chmod -R 755 /var/www/html/
	chown -R www-data:www-data /var/www/html/
	
fi  


clear

pkill zenity
zenity --progress  --title="Kane Installations-Assistent V$version""$aktivierungstext" \
  --text "Kane wurde erfolgreich installiert! Die Daten werden jetzt im Hintergrund aktualisiert, der Rechner wird nach Abschluss automatisch neugestartet"\
  --percentage=3  --width=410 \
  --pulsate \
  --no-cancel &

#aktivierungs notification
zenity --notification --text="Kane Installations-Assistent V$version""$aktivierungstext\nDie Installation von Kane ist nun abgeschlossen. Der Rechner wird in wenigen Minuten automatisch neugestartet. Bitte speichern Sie alle Dateien und schliessen Sie alle Programme."

echo "running vplan updatedb update"
wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php?force &> /dev/null &
wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php?force &> /dev/null
wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php?force &> /dev/null

chmod 777 '/var/www/html/vplan_touch/index.php' 

reboot
