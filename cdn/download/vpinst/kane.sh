#!/bin/bash
#(c)2016 Dominik Ziegenhagel (www.ziegenhagel.com)
version=4.3.2
																																																	
#conf
site="https://softatomos.com/kane"
cdnroot=$site"/cdn/download/"
tempdir="/tmp/vplantouch/"
cdnpath=$cdnroot"vpinst/res/"
zipfile="vplan_touch_v"$(curl --silent  $site/cdn/download/source/version/)".zip"
mozillafile="mozilla.zip"
installdir="/var/www/html/"
dbfile="vplan_touch/bin/dbcon.php"
sqlpath="vplan_touch/sql/"
sqlfile="webscheduler_structure.sql"
database="webscheduler"
lockfile="$tempdir"".lock"
maindlpath=$cdnroot"source/version/"$zipfile
updatedbfile="vplan_updatedb.zip"
vplanupdatedb=$site"/cdn/download/source/vplan_updatedb.zip"





# general actions
case $1 in
  pushv)
		echo "server version:"`curl --silent  $site/cdn/vpinst.v`
		read -s -p "Key?" key
		cd /tmp
		curl --silent $site"/cdn/vpinst_pushv.php?key="$key
		echo
		echo "server version:"`curl --silent  $site/vplantouch/vpinst.v`
		exit
     ;;
   u)
   	#update
	
		cd /tmp
		wget -c $site/cdn/download/vpinst/kane.sh
		chmod +x kane.sh
		sudo mv kane.sh /usr/bin/kane && sudo kane
        exit

     ;;
   v)
   	echo "Version: "$version
   	exit
     ;;
esac



#checking for coreect os
f="`cat /etc/lsb-release  | grep RELEASE`"
e="DISTRIB_RELEASE=18.1"
if [[ $e == $f ]]; then echo "OK"; else 

    echo "Wrong OS! Please download Linux Mint Mate 18.1 !";
    exit;

fi




#checking for root privilegs...

if [[ $EUID -ne 0 ]]; then
	sudo $0
	exit
fi



clear

echo "-----------------------------------------------------------------------"
echo "Kane Installer Dominik Ziegenhagel 2017 (www.ziegenhagel.com)"
echo "Version: $version"
echo "Temporary dir: $tempdir"
echo "-----------------------------------------------------------------------"
echo




echo "checking for internet connection..."
wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "[OK]"
else
    echo "[Failed]"
    echo "You're offline. Please connect to the internet"
    exit
fi




sudo apt install gawk -y



function version { echo "$@" | gawk -F. '{ printf("%03d%03d%03d\n", $1,$2,$3); }'; }
echo "checking for newer versoin"
serverversion=$(curl --silent  $site/cdn/vpinst.v)
if [ "$(version "$version")" -lt "$(version "$serverversion")" ]; then

			cd /tmp
			wget -c $site/cdn/download/vpinst/vpinst.sh
			chmod +x vpinst.sh
			sudo mv vpinst.sh /usr/bin/vpinst && sudo vpinst
			
			

#		if [ -f /tmp/vpinst_auto_update ]; then
#			read -p  "> there is a newer version of vpinst avaiable, may i update? (y/n)" ifupdate
#		else
#			cd /tmp
#			wget -c $site/cdn/download/vpinst/vpinst.sh
#			chmod +x vpinst.sh
#			sudo mv vpinst.sh /usr/bin/vpinst && sudo vpinst
#		fi


#		if [[ $ifupdate == "y" ]]; then
#
#			cd /tmp
#			wget -c $site/cdb/download/vpinst/vpinst.sh
#			chmod +x vpinst.sh
#			sudo mv vpinst.sh /usr/bin/vpinst && sudo vpinst
#
#		else
#			echo "exiting"
#			exit
#		fi


	exit
else
	echo "[OK]"
fi





#handling temporary dir
if [ -d "$tempdir" ]; then
	rm -r $tempdir
fi

mkdir $tempdir
cd $tempdir




read -p "> May I run update and upgrade first (recommended) ? (y/n)" proceed
if [[ $proceed == "y" ]]
then
	sudo apt-get update && sudo apt-get upgrade -y
fi 


read -p "> May I install packages (required) ? (y/n)" proceed
if [[ $proceed == "y" ]]
then
	sudo apt-get install wget openssh-server apache2 php7.0 mysql-server php7.0-curl php7.0-zip curl vim php libapache2-mod-php php-mcrypt php-mysql php7.0-xml xdotool -y
fi


#read -p "> Do you want me to set up mysql with phpmyadmin? (y/n)" proceed
#if [[ $proceed == "y" ]]
#then
#	sudo apt-get install phpmyadmin -y
#	
#	#get phpmyadmin an stuff rockin
#	
#	sudo echo "Include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf
#	echo "[OK]"
#	echo "include written to /etc/apache2/apache2.conf"
#	
#	echo "restarting apache ..."
#	sudo service apache2 restart
#	echo "[OK]"
#
#	
#fi 




notcorrect=true;


while [ $notcorrect ]
do
	
	echo "Please enter mysql connection data, press Enter for default value.";
	read -p "> hostname (default=localhost) :" hostname
	read -p "> username (default=root)      :" username
	read  -s -p "> password (not visible while typing):" password
	
	[[ $hostname == "" ]] && hostname="localhost"
	[[ $username == "" ]] && username="root"

	if mysql -u$username -p$password  -e 'show databases'; then
	    notcorrect=false
		 break;
	else
		echo "[Failed] Couldn't log into mysql, please check your connection data"
	fi
	
	
done







read -p "> Do you want me to download VPlan Touch (recommended) ? (y/n)" proceed
if [[ $proceed == "y" ]]
then


   	mv /var/www/html/vplan_touch/config/ /var/www/html/vplan_touch/tmp_config/ 
	
	barcodesdynamic="/var/www/html/vplan_touch/var/barcodes_dynamic.json_""$RANDOM";
	echo "creating backup of barcode files..."

	cp /var/www/html/vplan_touch/var/barcodes_dynamic.json $barcodesdynamic
	
	echo "Enviroment set up. Getting VPlan Touch ..."
	
	wget -c $maindlpath
	
	echo "Checking file"
	size=$( stat -c %s $zipfile)
	
	if [[ size -gt 50000 ]]; then
		echo "[OK]"
	else
		echo "[Failed]"
		exit
	fi
	
	rm /var/www/html/index.html
	
	echo "starting unzipping $installdir"
	unzip $zipfile -d $installdir
	echo "[OK]"

	echo "owning vplan_touch root to www-data"
	chown www-data -R /var/www/html/vplan_touch/
	
	
	echo "writing db connection data..."
	echo "<?php \$db=mysqli_connect(\"$hostname\",\"$username\",\"$password\",\"$database\"); ?>" > "$installdir$dbfile"
	echo "[OK]"
	
	rm  /var/www/html/vplan_touch/var/barcodes_dynamic.json
	mv $barcodesdynamic /var/www/html/vplan_touch/var/barcodes_dynamic.json


    read -p "> Use given config? (First time installing? Type 'n'. You changed the configuration manually before and want to keep it? Then type 'y' (y/n)" proceed
    if [[ $proceed == "y" ]]
    then
        rm -r /var/www/html/vplan_touch/config/
        mv /var/www/html/vplan_touch/tmp_config/ /var/www/html/vplan_touch/config/ 
    else
	    read -p "> Which language do you want your GUI? (de/en/fr/es)?" lang
	    [[ $lang == "" ]] && lang="de"
	    echo '<?php $school_config["conf"]["lang"]="'$lang'"; ?>' > /var/www/html/vplan_touch/config/tmp.school_config_generate_file_insert.php
	    	
	    read -p "> Please enter a conf server path (optional, press enter to skip)? " lang
	    [[ $lang == "" ]]
	    echo '<?php $school_config["conf"]["conf_server_path"]="'$lang'"; ?>' >> /var/www/html/vplan_touch/config/tmp.school_config_generate_file_insert.php
	    	
	    wget -qO- http://localhost/vplan_touch/config/school_config_generate_file.php?firstinstall &> /dev/null
    fi

fi

read -p "> Do you want me to install VPlan UpdateDB for getting the data from a server and VPlan ServerDB to host an API? (y/n)" proceed
if [[ $proceed == "y" ]]
then

	
	echo "owning vplan_touch root to www-data"
	chown www-data -R /var/www/html/vplan_updatedb/
	
	

	cd /var/www/html
	
	rm vplan_updatedb.zip

	wget -c $vplanupdatedb

	echo "getting "$vplanupdatedb
	
	echo "Checking file"
	sizeb=$( stat -c %s "vplan_updatedb.zip")
	
	if [[ sizeb -gt 10000 ]]; then
		echo "[OK]"
	else
		echo "[Failed]"
		exit
	fi
	
	echo "starting unzipping $updatedbfile"
	unzip $updatedbfile -d $installdir
	echo "[OK]"
	
	echo "owning vplan_updatedb root to www-data"
	chmod -R 777 /var/www/html/vplan_updatedb/
	chown www-data -R /var/www/html/vplan_updatedb/
	

	echo "checking crontab... Not found. adding crontab entry..."
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php &> /dev/null'>/tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php &> /dev/null'>>/tmp/vplantouch/cron_vplantouch_update
	echo '* * * * * wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php &> /dev/null'>>/tmp/vplantouch/cron_vplantouch_update
	echo "">>/tmp/vplantouch/cron_vplantouch_update
	chmod +x /tmp/vplantouch/cron_vplantouch_update
	crontab -u vplantouch /tmp/vplantouch/cron_vplantouch_update

	
	
	echo "Please enter untis api connection data, press Enter for default value.";
	read -p "> url (default=https://erato.webuntis.com/WebUntis/jsonrpc.do?school=ghse) :" untisurl
	[[ $untisurl == "" ]] && untisurl="https://erato.webuntis.com/WebUntis/jsonrpc.do?school=ghse"

	read -p "> username (default=ghse15)      :" untisusername
	[[ $untisusername == "" ]] && untisusername="ghse15"
	read -s -p "> password (not visible while typing):" untispassword
	
	
	

	read -p "> (optional) Do you want to addtionally save login data for retrieving the api connection data from a json file ? (y/n):" proceed
	
	if [[ $proceed == "y" ]]
	then
		read -p "> url (default=https://www.ghse.de/vpapp/ghse.json) :" untisurl2
		read -p "> username (default=ghse15)      :" untisusername2
		read -s -p "> password (not visible while typing):" untispassword2
		[[ $untisurl2 == "" ]] && untisurl2="https://www.ghse.de/vpapp/ghse.json"
		[[ $untisusername2 == "" ]] && untisusername2="ghse15"
	

	fi



	echo "{\"Login-Server\": {\"Username\": \"$untisusername2\",\"Password\": \"$untispassword2\",\"Host\":\"$untisurl2\"},\"Database\": {\"Username\": \"$username\",\"Password\": \"$password\",\"Database-Name\": \"$database\",\"Host\":\"$hostname\"},\"Data-Server\": {\"Url\":\"$untisurl\",\"Username\": \"$untisusername\",\"Password\": \"$untispassword\"}}" > "$installdir""vplan_updatedb/etc/logins.json"




awk '

# alles zwischen <Directory  und /<\/Directory>/
/<Directory/, /<\/Directory>/ {

# Esretze sie string in $0 = ganze Zeile

                if ( $0 == "<Directory /var/www/>" )            # Eintrag gefunden, um den es geht.
                {
                        DOIT="yes" ;                            # Merken !
                        printf "found line %d\n", NR;           # debug
                }

                if ( DOIT == "yes" )
                {
                        res = sub("AllowOverride None", "AllowOverride AuthConfig", $0 ) ;      # res = 0, wenn nicht ausgetauscht wurde, sonst 1
                        if ( res == 1 ) # Austausch hat stattgefunden
                        {
                                printf "exchanged line %d / %d\n", NR, res; # debug
                                DOIT="no" ;
                        }
                }

        # Zeile Ausgeben
        print

        # auf zur n�chsten Zeile
        next
}

# alle anderern Zeile einfach ausgeben
{ print ; }
' /etc/apache2/apache2.conf > mktemp

diff /etc/apache2/apache2.conf mktemp

echo cp mktemp /etc/apache2/apache2.conf

rm mktemp

sudo a2enmod rewrite
sudo service apache2 restart



	
fi













read -p "> Do you want me to setup the database structure (highly recommended)? (y/n)" proceed
if [[ $proceed == "y" ]]
then
	
	cd $installdir$sqlpath
	
	mysql -u$username -p$password -e "drop database $database";
	mysql -u$username -p$password -e "drop database webschedulertmp";
	mysql -u$username -p$password -e "create database $database";
	mysql -u$username -p$password -e "create database webschedulertmp";
	mysql -u$username -p$password --database=$database -e "source $sqlfile";
	mysql -u$username -p$password --database=webschedulertmp -e "source $sqlfile";
	
	echo "[OK]"

fi 


cd $tempdir


echo "VPlan Touch now should be working fine. "


read -p "> Do you want me to turn your computer into a VPlan Touch PC?(recommended for production state) (y/n)" proceed
if [[ $proceed == "y" ]]
then
		
		
	ret=true
	getent passwd vplantouch >/dev/null 2>&1 && ret=false
	
	if $ret; then
	
		echo "I'll set up a new user named vplantouch, please choose a password for this user when youre being asked for it."
		read -p "> Please press enter and choose a new password for the vplantouch user"
		
		sudo adduser vplantouch
				
	fi	
		



	read -p "> Do you want to enable automatic updates of VPlan Touch too (recommended) ? (y/n)" proceeed
	if [[ $proceeed == "y" ]]
	then

		echo "checking crontab... Not found. adding crontab entry..."
		echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php &> /dev/null' > /tmp/vplantouch/cron_vplantouch_update
		echo '* * * * * wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php &> /dev/null' >> /tmp/vplantouch/cron_vplantouch_update
		echo '* * * * * wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php &> /dev/null' >> /tmp/vplantouch/cron_vplantouch_update
		echo "" >> /tmp/vplantouch/cron_vplantouch_update
		chmod +x /tmp/vplantouch/cron_vplantouch_update
		crontab -u vplantouch /tmp/vplantouch/cron_vplantouch_update
	fi

	pkill firefox
	
	cd /home/vplantouch
	
	mv .mozilla .mozilla_
	
	wget -c "$cdnpath$mozillafile"
	unzip $mozillafile
	rm $mozillafile
	chown -R vplantouch .mozilla
	
	wget -c https://softatomos.com/vplantouch/cdn/download/vpinst/res/firefox_bootstrap.sh.zip
    unzip firefox_bootstrap.sh.zip
	chmod +x .firefox_bootstrap.sh
	chown -R vplantouch .firefox_bootstrap.sh
	

	echo "writing config"
	cd /home/vplantouch
	mv .config .config_
	wget -c "$cdnpath""config.zip"
	unzip config.zip
	rm config.zip
	chown -R vplantouch .config
	
	echo "setting up autologin"
	echo "
[daemon]

AutomaticLoginEnable=true
AutomaticLogin=vplantouch

[security]

[xdmcp]

[gui]

[greeter]

[chooser]

[debug]

[servers]
">>/etc/mdm/mdm.conf 


	cd $tempdir
	
	echo "changeing background"
	wget -c "$cdnpath""bg.jpg"
	
	cp "bg.jpg" /usr/share/backgrounds/linuxmint/linuxmint.jpg
	cp "bg.jpg" /usr/share/backgrounds/linuxmint/default_background.jpg

	echo "removing desktop icons"
	
	echo "setting bot logo"
	wget -c "$cdnpath""logo.png"
	sudo apt-get install startupmanager plymouth plymouth-drm plymouth-themes-mint -y
	sudo /usr/sbin/plymouth-set-default-theme mint-logo
	cp logo.png /lib/plymouth/themes/mint-logo/logo.png
	sudo update-initramfs -u
	

	mv Desktop Desktop_
	mv Schreibtisch Schreibtisch_
	
	chmod -R 755 /var/www/html/
	chown -R www-data:www-data /var/www/html/
	
fi


clear
echo "VPlan Touch was installed. Executing cron"


echo "running vplan updatedb update"
wget -qO- http://localhost/vplan_updatedb/bin/cron_untis.php?force &> /dev/null
echo "fetching files"
wget -qO- http://localhost/vplan_updatedb/bin/cron_update.php?force &> /dev/null
echo "running vplan touch update"
wget -qO- http://localhost/vplan_touch/bin/cron_check_update.php?force &> /dev/null

chmod 777 '/var/www/html/vplan_touch/index.php' 

read -p "> Do you want to restart your Computer now? (y/n)" ifrestart

echo "
Have a nice day!"

if [[ $ifrestart == "y" ]]
then
	reboot
fi
