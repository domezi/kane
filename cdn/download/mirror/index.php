<h1>VPlan Touch Mirror</h1><hr>
<?php
include("config.php");
$lastchange=0;
foreach(glob("mirror/*") as $file) {
    if(!strstr($file,"bkup") && !strstr($file,"index.php")) {
        echo "<a href=$file>".str_replace("mirror/","",$file)."</a>";
        $lastchange=filemtime($file);
    }
}
?>
<hr>
Last update: <?php echo date("d.m.Y H:i", $lastchange); ?>
<style>
* {
    font-family:arial,ubuntu;
    color:#444;
    font-weight:100;
}
hr {
    border:none;
    background:none;
    border-bottom:1px solid #ddd;
}
body {
    padding:40px;
}
a {
    display:block;
    padding:5px;
}
a:hover {
    background:#eee;
}
</style>
