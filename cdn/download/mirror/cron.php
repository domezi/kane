<?php


        /* source code released under public domain license  by dominik ziegenhagel 2017 */

        // cronjob - please do not change

        // load config
        include("config.php");
        
        // don't change !
        $dir="mirror/";

        error_reporting(-1);

        // create curl resource
        $ch = curl_init();

        // set url so we know where to find you 
        $mirror="http://".$_SERVER['SERVER_NAME'].str_replace("cron.php","",$_SERVER["REQUEST_URI"]).$dir;

        curl_setopt($ch, CURLOPT_URL, "http://www.vplantouch.com/cdn/download/source/version/download_path.php?mirror_server_url=".$mirror);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //set user agent
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);    
        
        // set stuff to backup
        $urls=array("vpinst.sh"=>"http://www.vplantouch.com/cdn/download/vpinst/vpinst.sh",
            "vplan_touch.zip"=>$output,
            "vplan_updatedb.zip"=>"http://www.vplantouch.com/cdn/download/source/vplan_updatedb.zip");

        print_r($urls);

        // create dir if necessary
        if(!is_dir($dir))mkdir($dir);
        
        // backup every script
        foreach($urls as $filename=>$url) {
            
            if(is_numeric($filename))continue;

            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            //set user agent
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

            // $output contains the output string
            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);    

            // delete oldest version 
            unlink($dir."bkup".$backups."_".$filename);
            
            // go through versions an push them one version 
            for($i=$backups;$i>0;$i--) {
                rename($dir."bkup".$i."_".$filename,$dir."bkup".($i+1)."_".$filename);
            }
    
            //get neweset version out of the way
            rename($dir.$filename,$dir."bkup1_".$filename);
        
            // save new version
            file_put_contents($dir.$filename,$output);

            echo $url."<br>";

        }

?>
